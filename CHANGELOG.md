# Changelog

## [AnonAddy v1.1.3] - 2020-12-08

### Fixed/Improved
- 🐛 [BUG FIX] Deleted alias section is not collapsed by default
- 🙋 [FEATURE REQUEST] Switch between panels by swiping left/right


## [AnonAddy v1.1.2] - 2020-11-21

### Fixed/Improved
-  🖥️ Optimized UI for big screens
-  🐛 [BUG FIX] Improved responsiveness when switching fragments
-  🙋 [FEATURE REQUEST] Added link to gitlab page
-  🙋 [FEATURE REQUEST] Separated deleted aliases into a different section


## [AnonAddy v1.1.1] - 2020-10-24

### Fixed/Improved
-  Added the forgotten piece of the custom alias format >.<
-  Fixed crash at double Biometrics authentication prompt when dark mode was enabled


## [AnonAddy v1.1.0] - 2020-10-24

### Added
-  📋 Added new rule-editor (beta)
    -  The feature is still in beta, and might not be available on the hosted instance
    -  For self-hosted instanced, enable the rule feature in order to use this editor
-  🔤 Added catch-all switch for AnonAddy v0.4.0>
-  ⌨ Added custom alias format option
-  💸 Added subscription check for random words alias format
-  💌 Show changelog on update
-  🛠️ Added version check (for self hosted instances)


### Fixed/Improved
-  🔎 Improved the search function
-  🌟 UI improvements